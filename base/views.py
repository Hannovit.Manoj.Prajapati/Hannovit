from django.views.generic import TemplateView,ListView
from django.shortcuts import render
import os
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.http import JsonResponse
from django.core.mail import send_mail
from django.conf import settings
from . import bussiness




# Create your views here.


# this view for Home Page
import os
import json
from django.conf import settings
from django.views.generic import ListView

class Home(ListView):
    template_name = "index.html"
    context_object_name = 'data'

    def get_queryset(self):
        blog_file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'blog_details.json')
        service_file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'service.json')

        with open(blog_file_path, encoding="utf8") as blog_file, open(service_file_path, encoding="utf8") as service_file:
            blogs = json.load(blog_file)
            services = json.load(service_file)

        context_data = {
            'blogs': blogs[:3],
            'services': services 
        }

        return context_data


# this view for About
class About(TemplateView):
    template_name = "about.html"

#this view for Service
class Service(ListView):
    template_name = 'service.html'
    context_object_name = 'services'
    def get_queryset(self):
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'service.json')
        with open(file_path, encoding="utf8") as f:
            services = json.load(f)
        return services



class SerivceDetail(TemplateView):
    template_name = 'service_detail.html'

    def get(self,request,*args,**kwargs):
        id = self.kwargs.get('id')
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'service.json')
        return bussiness.get_content(request, id, self.template_name, file_path, 'service')

    
#BlogList for displaying all blogs
class BlogList(ListView):
    template_name = 'blog_list.html'
    context_object_name = 'blogs'
    def get_queryset(self):
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'blog_details.json')
        with open(file_path, encoding="utf8") as f:
            blogs = json.load(f)
        return blogs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = self.request.GET.get('page')
        paginator = Paginator(context['blogs'],9)

        try:
            blogs = paginator.page(page)
        except PageNotAnInteger:
            blogs = paginator.page(1)
        except EmptyPage:
            blogs = paginator.page(paginator.num_pages)

        context['blogs'] = blogs
        return context

class BlogDetail(TemplateView):
    template_name = 'blog_detail.html'

    def get(self, request, *args, **kwargs):
        blog_id = self.kwargs.get('id')
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'blog_details.json')
        return bussiness.get_content(request, blog_id, self.template_name, file_path, 'blog')
        



class Portfolio(TemplateView):
    template_name = 'portfolio.html'

class PortfolioDetail(TemplateView):
    template_name = 'portfolio_detail.html'

    def get(self,request,*args,**kwargs):
        portfolio_id = self.kwargs.get('id')
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'portfolio.json')
        return bussiness.get_content(request, portfolio_id, self.template_name,  file_path, 'portfolio')

class Contact(TemplateView):
    template_name = 'contact.html'

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        email = request.POST.get('email')
        content = request.POST.get('content')
        subject = request.POST.get('subject')
        recipient_list = [settings.EMAIL_HOST_USER, ]
        try:
            send_mail(subject, content, email, recipient_list)
            response_data = {'success': True}
            
        except Exception as e:
            response_data = {'success': False, 'error_message': str(e)}
            print("what is eroor:",response_data.error_message)
        return JsonResponse(response_data)

class Team(TemplateView):
    template_name = 'team.html'

class Industries(TemplateView):
    template_name = 'industries.html'



class IndustriesDetail(TemplateView):
    template_name = 'industries_detail.html'

    def get(self, request, *args, **kwargs):
        industries_id = self.kwargs.get('id')
        file_path = os.path.join(settings.BASE_DIR, 'static', 'json', 'industries.json')
        return bussiness.get_content(request, industries_id, self.template_name, file_path, 'industries')

class Privacy(TemplateView):
    template_name = 'privacy.html'

class Term(TemplateView):
    template_name = 'term.html'


class RobotsTxtView(TemplateView):
    template_name = "robots.txt"