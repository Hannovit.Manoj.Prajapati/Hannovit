
!(function (e) {
  "use strict";
    e(window).scroll(function () {
      e(this).scrollTop() > 45
        ? e(".navbar").addClass("sticky-top shadow-sm")
        : e(".navbar").removeClass("sticky-top shadow-sm");
    });
    e('[data-toggle="counter-up"]').counterUp({ delay: 10, time: 2e3 }),
    e(window).scroll(function () {
      e(this).scrollTop() > 100
        ? e(".back-to-top").fadeIn("slow")
        : e(".back-to-top").fadeOut("slow");
    }),
    e(".back-to-top").click(function () {
      return (
        e("html, body").animate({ scrollTop: 0 }, 1500, "easeInOutExpo"), !1
      );
    }),
    e(".testimonial-carousel").owlCarousel({
      autoplay: !0,
      smartSpeed: 1500,
      dots: !0,
      loop: !0,
      center: !0,
      responsive: {
        0: { items: 1 },
        576: { items: 1 },
        768: { items: 2 },
        992: { items: 3 },
      },
    }),
    e(".vendor-carousel").owlCarousel({
      loop: !0,
      margin: 45,
      dots: !1,
      loop: !0,
      autoplay: !0,
      smartSpeed: 1e3,
      responsive: {
        0: { items: 2 },
        576: { items: 4 },
        768: { items: 6 },
        992: { items: 8 },
      },
    });
})(jQuery),
  $(document).ready(function () {
    $("form").submit(function (e) {
      e.preventDefault(),
        $.ajax({
          type: "POST",
          url: "",
          data: $("form").serialize(),
          success: function (e) {
            e.success
              ? $("#successMessage").show()
              : alert("Error sending message: " + e.error);
          },
          error: function (e) {
            alert("An unexpected error occurred."+e.error);
          },
        });
    });
  });


  $(document).ready(function() {
   
    if (document.URL.includes("contact")) {
      $('html, body').animate({
        scrollTop: $('#contactForm').offset().top
      }, 'slow');
    }
  });